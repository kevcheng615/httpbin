.PHONY: pip-install lint test serve
pip-install:
	pip3 install -r requirements.txt
lint:
	flake8 --extend-ignore=W605,E722,E501,F401,F403 httpbin
test:
	python3 test_httpbin.py
serve:
	gunicorn -b 0.0.0.0:5000 -k gevent httpbin:app
